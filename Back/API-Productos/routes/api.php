<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Controllers\clienteController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('clientes','App\Http\Controllers\clienteController');
Route::resource('proveedores','App\Http\Controllers\provedorController');
Route::resource('productos','App\Http\Controllers\productoController');
Route::resource('vendedores','App\Http\Controllers\vendedorController');
Route::resource('ventas','App\Http\Controllers\ventaController');
Route::resource('detalleventas','App\Http\Controllers\detalleVentaController');