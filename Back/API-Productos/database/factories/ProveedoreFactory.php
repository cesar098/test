<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProveedoreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nombre' => fake()->firstName(),
            'direccion' => fake()->streetAddress(),
            'rut' => fake()->numberBetween(2310121,2999999),
            'pagina_web' => 'www.'.fake()->lastName().'.com',
        ];
    }
}
