<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Producto>
 */
class ProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id_proveedor' => fake()->numberBetween(1,99),
            'nombre' => fake()->randomElement(['towel', 'needle', 'candy wrapper', 'cell phone', 'spoon','bag', 'mouse', 'air freshener', 'pencil', 'clock','controller', 'carrots', 'blanket', 'monitor', 'mp3 player']),
            'precio' => fake()->numberBetween(100,1500),
            'stock' => fake()->numberBetween(5,50)
        ];
    }
}
