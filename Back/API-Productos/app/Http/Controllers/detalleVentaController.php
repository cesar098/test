<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Detalle_venta;

class detalleVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Detalle_venta::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $detaelle_venta = new Detalle_venta();
        $detaelle_venta->id_venta = $request->id_venta;
        $detaelle_venta->id_producto = $request->id_producto;
        $detaelle_venta->cantidad = $request->cantidad;
        $detaelle_venta->subtotal = $request->subtotal;

        $detaelle_venta->save();
        return $detaelle_venta;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Detalle_venta::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $detaelle_venta = Detalle_venta::findOrFail($id);
        $detaelle_venta->id_venta = $request->id_venta;
        $detaelle_venta->id_producto = $request->id_producto;
        $detaelle_venta->cantidad = $request->cantidad;
        $detaelle_venta->subtotal = $request->subtotal;

        $detaelle_venta->save();
        return $detaelle_venta;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Detalle_venta::destroy($id);
    }
}
