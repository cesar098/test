<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendedore extends Model
{
    use HasFactory;
    protected $fillable = ['nombre','apellidos','direccion','telefono','rut','fecha_nac','email'];
}
