## Instalacion

Para realizar la instalacion, debe terner instaldo php mayor a 8.1, composer mayor a 2.2 y mysql

## Instrucciones

Clonar el reporsitorio en su maquina local

```
mkdir Back
cd Back
git clone https://gitlab.com/cesar098/test.git

```

Con el proyecto clonado se debe instalar las dependencias y cambiar el archivo .env ingresando el nombre de la base de datos y las credenciales

```
composer install

//config DataBase
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=api_productos
DB_USERNAME=root
DB_PASSWORD=1234

```

Despues se debe crear la base de datos y ejecutar las migraciones para que se creen las respectivas tablas junto a los seeders

```
php artisan migrate
php artisan db:seed --class=ProductoSeeder
php artisan db:seed --class=VendedorSeeder
php artisan db:seed --class=ClienteSeeder
php artisan db:seed --class=ProveedorSeeder

```

Como ulltimo iniciar el la aplicacion con el siguiente comando

```

php artisan serve

```

